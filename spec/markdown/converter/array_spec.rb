require 'spec_helper'

describe Array do
  context 'to_md_checklist' do
    it 'should create a multiline checklist' do
      input = %w(one two three)
      expect(input.to_md_checklist).to eq(<<-EOF
- [ ]: one
- [ ]: two
- [ ]: three
EOF
                                         )
    end

    it 'should create a sublist out of subarrays' do
      input = %w(one two three)
      sub_input = %w(four five six)
      expect(input.insert(1, sub_input).to_md_checklist).to eq(<<-EOF
- [ ]: one
   - [ ]: four
   - [ ]: five
   - [ ]: six
- [ ]: two
- [ ]: three
EOF
                                                              )
    end
  end
end
