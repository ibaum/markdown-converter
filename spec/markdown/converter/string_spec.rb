require 'spec_helper'

describe String do
  context 'to_md_checklist' do
    it 'should create a one entry checklist' do
      expect("I'm an item in a checklist".to_md_checklist).to eq(
        "- [ ]: I'm an item in a checklist\n"
      )
    end
  end
end
