require 'spec_helper'

describe Hash do
  context 'to_md_checklist' do
    it 'should create a basic checklist' do
      foo = {
        one: %w(two three)
      }
      expect(foo.to_md_checklist).to eq(<<-EOF
- [ ]: one
   - [ ]: two
   - [ ]: three
EOF
                                       )
    end
  end
end
