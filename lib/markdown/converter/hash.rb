class Hash
  def to_md_checklist(indent_level = 0)
    md = ''
    each_pair do |key, val|
      md << "#{LIST_PREFIX} #{key}\n"
      md << val.to_md_checklist(indent_level + 1)
    end
    md
  end
end
