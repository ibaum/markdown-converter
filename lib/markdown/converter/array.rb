class Array
  def to_md_checklist(indent_level = 0)
    md = ''
    each do |val|
      md << if val.class == Array
              val.to_md_checklist(indent_level + 1)
            else
              val.to_md_checklist(indent_level)
            end
    end
    md
  end
end
