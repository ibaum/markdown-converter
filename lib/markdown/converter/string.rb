class String
  def to_md_checklist(indent_level = 0)
    "#{INDENT_PREFIX * indent_level}#{LIST_PREFIX} #{self}\n"
  end
end
