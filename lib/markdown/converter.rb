require 'markdown/converter/version'
require 'markdown/converter/array'
require 'markdown/converter/string'
require 'markdown/converter/hash'

LIST_PREFIX = '- [ ]:'
INDENT_PREFIX = '   '
